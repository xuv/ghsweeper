var config = {};

config.github = {};
config.mongo = {};

config.last_id = 0;

// Fill in your github user and password
config.github.user = process.env.GITHUB_USER || 'username';
config.github.password=  process.env.GITHUB_PASSWORD || 'password';

// Change relevant info for your Mongo database
config.mongo.host = process.env.MONGO_HOST || '127.0.0.1'; 
config.mongo.port = process.env.MONGO_PORT || 27017;
config.mongo.db = process.env.MONGO_DB || 'db_name'; 
config.mongo.user = process.env.MONGO_USER || 'username';
config.mongo.password = process.env.MONGO_PASSWORD || 'password';

module.exports = config;